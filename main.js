
// #ifndef VUE3
import Vue from 'vue'
import App from './App'

//导入网络请求的包
import { $http } from '@escook/request-miniprogram'
import store from './store/store.js'

uni.$http = $http

//这是请求的根路径
$http.baseUrl = `https://www.fastmock.site/mock/215620a746929d9642716aaa08261b60/ua`

//请求拦截器
$http.beforeRequest = function(options){
  // console.log(options)
  uni.showLoading({
    title: '数据加载中。。。。'
  })
  
  // 判断请求头是否为有权限的api接口
  if(options.url.indexOf('/my/') !== -1){
    //为请求头添加身份认证
    options.header = {
      //字段的值可以从store中获取
      Authorization: store.state.m_user.token,
    }
  }
  // console.log(options)
}

//响应拦截器
$http.afterRequest = function(options){
  uni.hideLoading()
}

//封装showToast函数
uni.$ShowMsg = function(title='数据加载失败', duration=1500){
  uni.showToast({
    title,
    duration,
    icon: 'none'
  })
}

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App,
    // 将store挂载在vue实例上
    store,
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif