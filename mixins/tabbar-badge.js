import {
  mapGetters
} from 'vuex'

export default {
  computed: {
    ...mapGetters('m_cart', ['total'])
  },
  
  watch: {
    total(){
      this.setBadge()
    }
  },

  onShow() {
    // 在页面刚展示的时候，设置数字徽标
    this.setBadge()
    // console.log(this.total)
  },

  methods: {
    setBadge() {
      uni.setTabBarBadge({
        index: 2,
        text: this.total + ''
      })
    }
  }
}
