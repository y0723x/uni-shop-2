export default {
  namespaced: true,
  
  state: () => ({
    //收货地址
    address: JSON.parse(uni.getStorageSync('address') || '{}'),
    //登陆成功后的字符串
    token: uni.getStorageSync('token' || ''),
    //用户信息
    userinfo: JSON.parse(uni.getStorageSync('userinfo')  || '{}'),
    //重定向的object对象 {openType, from}
    redirectInfo: null,
  }),
  
  mutations: {
    //更新收货地址
    updateAddress(state, address){
      state.address = address
      this.commit('m_user/saveAddressToStorage')
    },
    
    //将address持久化存储至本地
    saveAddressToStorage(state){
      uni.setStorageSync('address', JSON.stringify(state.address))
    },
    
    //更新用户基本信息
    updateUserInfo(state, userinfo){
      state.userinfo = userinfo
      this.commit('m_user/saveUserInfoToStorage')
    },
    
    //将userinfo保存保存到本地
    saveUserInfoToStorage(state){
      uni.setStorageSync('m_user', JSON.stringify(state.userinfo))
    },
    
    //更新token字符串
    updateToken(state, token){
      state.token = token
      this.commit('m_user/saveTokenToStorage')
    },
    
    //将token持久化存储至本地
    saveTokenToStorage(state){
      uni.setStorageSync('token', state.token)
    },
    
    //更新重定向对象
    updateRedirectInfo(state, info){
      state.redirectInfo = info
    }
  },
  
  getters: {
    //收获详细地址的计算属性
    addstr(state){
      if(!state.address.provinceName){
        return ''
      }
      return state.address.provinceName + state.address.cityName + state.address.countyName + state.address.detailInfo
    }
  }
}