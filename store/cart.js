export default {
  //为当前模块开启命名空间
  namespaced: true,

  //模块的state数据
  state: () => ({
    // 购物车数组，用来存储购物车中每个商品的信息对象
    // 每个商品的信息对象，都包含如下 6 个属性：
    // { goods_id, goods_name, goods_price, goods_count, goods_small_logo, goods_state }
    cart: JSON.parse(uni.getStorageSync('cart') || '[]')
  }),

  //模块的mutation方法
  mutations: {
    //加入购物车
    addToCart(state, goods) {
      // 根据提交的商品的Id，查询购物车中是否存在这件商品
      // 如果不存在，则 findResult 为 undefined；否则，为查找到的商品信息对象
      const findResult = state.cart.find((x) => x.goods_id === goods.goods_id)
      
      if(!findResult){
        // 如果没有这个商品，就直接添加
        state.cart.push(goods)
      } else {
        // 如果购物车有这个商品，则更新数量
        findResult.goods_count++
        // state.cart.find((x) => x.goods_id === goods.goods_id).goods_count++
        
        //通过commit方法，调用m_cart命名空间下的saveToStorage方法
        this.commit('m_cart/saveToStorage')
      }
    },
    
    //将购物车的数据持久化的存储到本地
    saveToStorage(state){
      uni.setStorageSync('cart', JSON.stringify(state.cart))
    },
    
    //修改购物车商品状态
    updateGoodsState(state, goods){
      const findResult = state.cart.find((x) => x.goods_id === goods.goods_id)
      
      if(findResult){
        findResult.goods_state = goods.goods_state
        this.commit('m_cart/saveToStorage')
      }
    },
    
    //修改购物车商品数量
    updateGoodsCount(state, goods){
      const findResult = state.cart.find((x) => x.goods_id === goods.goods_id)
      
      if(findResult){
        findResult.goods_count = goods.goods_count
        this.commit('m_cart/saveToStorage')
      }
    },
    
    //根据Id从购物车中删除对应的商品信息
    removeGoodsById(state, goods){
      state.cart = state.cart.filter((x) =>x.goods_id !== goods.goods_id)
      this.commit('m_cart/saveToStorage')
    },
    
    //全选点击事件
    updateAllGoodsState(state, newState){
      state.cart.forEach((x) => x.goods_state = newState)
      
      this.commit('m_cart/saveToStorage')
    }
    
  },

  //模块的getters方法
  getters: {
    //统计购物车商品数量
    total(state){
      let c = 0
      state.cart.forEach((goods) => c += goods.goods_count)
      return c
    },
    
    //统计已勾选商品数量
    checkedCount(state){
      //filter筛选出已勾选的商品
      //reduce返回已勾选商品的总数量count
      return state.cart.filter((x) => x.goods_state).reduce((total, item) => total += item.goods_count, 0)
    },
    
    //统计已勾选商品的总价格
    checkedGoodsAmount(state){
      return state.cart.filter((x) => x.goods_state).reduce((total, item) => total += item.goods_count * item.goods_price, 0).toFixed(2)
    }
  },
}
